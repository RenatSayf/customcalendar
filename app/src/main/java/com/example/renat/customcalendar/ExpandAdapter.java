package com.example.renat.customcalendar;

import android.content.Context;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.ArrayList;

/**
 * Created by Renat on 16.03.2017.
 */

public class ExpandAdapter extends BaseExpandableListAdapter
{
    private Context context;
    private ArrayList<ArrayList<Event>> groups;

    public ExpandAdapter (Context context, ArrayList<ArrayList<Event>> groups)
    {
        this.groups = groups;
        this.context = context;
    }

    @Override
    public int getGroupCount()
    {
        return groups.size();
    }

    @Override
    public int getChildrenCount(int i)
    {
        return groups.get(i).size();
    }

    @Override
    public Object getGroup(int i)
    {
        return groups.get(i);
    }

    @Override
    public Object getChild(int i, int i1)
    {
        return groups.get(i).get(i1);
    }

    @Override
    public long getGroupId(int i)
    {
        return i;
    }

    @Override
    public long getChildId(int i, int i1)
    {
        return i1;
    }

    @Override
    public boolean hasStableIds()
    {
        return true;
    }

    @Override
    public View getGroupView(int position, boolean isExpanded, View view, ViewGroup viewGroup)
    {
        View convertView = view;
        if (convertView == null)
        {
            LayoutInflater systemService = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = systemService.inflate(R.layout.group_view, viewGroup, false);
        }
        ImageView imageView = (ImageView) convertView.findViewById(R.id.icon_of_group);
        TextView groupNameView = (TextView) convertView.findViewById(R.id.group_name_text_view);

        String groupName = groups.get(position).get(0).getGroup();
        groupNameView.setText(groupName);

        if (groupNameView.getText().toString().equals(EventsList.groupsName[0]))
        {
            imageView.setImageResource(R.drawable.ic_break_fast);
        }
        if (groupNameView.getText().toString().equals(EventsList.groupsName[1]))
        {
            imageView.setImageResource(R.drawable.ic_sport_eating);
        }
        if (groupNameView.getText().toString().equals(EventsList.groupsName[2]))
        {
            imageView.setImageResource(R.drawable.ic_measuring);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isExpanded, View view, ViewGroup viewGroup)
    {
        View convertView = view;
        if (convertView == null)
        {
            LayoutInflater systemService = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = systemService.inflate(R.layout.child_view, viewGroup, false);
        }

        TextView eventNameView = (TextView) convertView.findViewById(R.id.event_name_text_view);
        TextView timeView = (TextView) convertView.findViewById(R.id.time_text_view);

        String name = groups.get(groupPosition).get(childPosition).getName();
        eventNameView.setText(name);
        String time = groups.get(groupPosition).get(childPosition).getTime();
        timeView.setText(time);

        final ToggleButton toggleBtn = (ToggleButton) convertView.findViewById(R.id.toggle_btn);

        toggleBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (toggleBtn.isChecked())
                {
                    toggleBtn.setBackground(context.getResources().getDrawable(R.drawable.toggle_on_background));
                    Toast.makeText(context, "Вкл", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    toggleBtn.setBackground(context.getResources().getDrawable(R.drawable.toggle_off_background));
                    Toast.makeText(context, "Откл", Toast.LENGTH_SHORT).show();
                }

            }
        });

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1)
    {
        return false;
    }
}
