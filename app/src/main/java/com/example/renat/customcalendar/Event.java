package com.example.renat.customcalendar;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by Renat on 17.03.2017.
 */

public class Event
{
    private String group;
    private String name;
    private String time;

    public Event(String group, String name, String time)
    {
        this.group = group;
        this.name = name;
        this.time = time;
    }

    public String getGroup()
    {
        return group;
    }

    public String getName()
    {
        return name;
    }

    public String getTime()
    {
        return time;
    }


}
