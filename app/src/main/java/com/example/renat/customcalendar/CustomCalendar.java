package com.example.renat.customcalendar;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by Renat on 13.03.2017.
 */

public class CustomCalendar extends BaseAdapter implements View.OnClickListener
{
    private final Context context;
    private final List<String> list;
    private static final int DAY_OFFSET = 1;
    private final String[] weekdays = new String[]{"Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"};
    public static final String[] months = {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"};
    private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int currentDayOfMonth;
    private int currentWeekDay;
    private int year;
    private EventsList eventsList;
    private ExpandableListView expandableListView;

    public CustomCalendar(Context context, int month, int year, ExpandableListView expandableListView)
    {
        super();
        this.context = context;
        this.list = new ArrayList<>();
        this.year = year;

        java.util.Calendar calendar = java.util.Calendar.getInstance();
        setCurrentDayOfMonth(calendar.get(java.util.Calendar.DAY_OF_MONTH));
        setCurrentWeekDay(calendar.get(java.util.Calendar.DAY_OF_WEEK));

        printMonth(month, year);

        eventsList = new EventsList();
        this.expandableListView = expandableListView;
    }

    private void printMonth(int mm, int yy)
    {
        int trailingSpaces;
        int daysInPrevMonth;
        int prevMonth;
        int prevYear;
        int nextMonth;
        int nextYear;
        int currentMonth = mm - 1;

        int daysInMonth = getNumberOfDaysOfMonth(currentMonth);

        GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 0);

        if (currentMonth == 11)
        {
            prevMonth = currentMonth - 1;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            nextMonth = 0;
            prevYear = yy;
            nextYear = yy + 1;
        }
        else if (currentMonth == 0)
        {
            prevMonth = 11;
            prevYear = yy - 1;
            nextYear = yy;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            nextMonth = 1;
        }
        else
        {
            prevMonth = currentMonth - 1;
            nextMonth = currentMonth + 1;
            nextYear = yy;
            prevYear = yy;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
        }

        trailingSpaces = cal.get(java.util.Calendar.DAY_OF_WEEK) - 1;

        if (cal.isLeapYear(cal.get(java.util.Calendar.YEAR)) && mm == 1)
        {
            ++daysInMonth;
        }

        // Trailing Month days
        for (int i = 0; i < trailingSpaces; i++)
        {
            list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-" + getMonthAsString(prevMonth) + "-" + prevYear+ "-GREY");
        }

        // Current Month Days
        for (int i = 1; i <= daysInMonth; i++)
        {
            if (i == getCurrentDayOfMonth())
            {
                list.add(String.valueOf(i) + "-" + getMonthAsString(currentMonth) + "-" + yy + "-BLUE");
            }
            else
            {
                list.add(String.valueOf(i) + "-" + getMonthAsString(currentMonth) + "-" + yy+ "-BLACK" );
            }
        }

        // Leading Month days
        for (int i = 0; i < list.size() % 7; i++)
        {
            list.add(String.valueOf(i + 1) + "-" + getMonthAsString(nextMonth) + "-" + nextYear + "-GREY");
        }
    }

    private String getMonthAsString(int i)
    {
        return months[i];
    }

    private String getWeekDayAsString(int i)
    {
        return weekdays[i];
    }

    private int getNumberOfDaysOfMonth(int i)
    {
        if(year % 4 == 0 && year % 100 != 0 || year % 400 == 0)
        {
            if (i == 1)
            {
                return 29;
            }
        }
        return daysOfMonth[i];
    }

    @Override
    public void onClick(View view)
    {
        String date_month_year = (String) view.getTag();
        TreeMap<String, ArrayList<ArrayList<Event>>> shedule = eventsList.getEventsShedule();
        ArrayList<ArrayList<Event>> arrayList = shedule.get(date_month_year);

        ExpandAdapter expandAdapter;
        if (arrayList != null)
        {
            expandAdapter = new ExpandAdapter(context, arrayList);
            expandableListView.setAdapter(expandAdapter);
        }
        else
        {
            expandableListView.removeAllViewsInLayout();
        }
    }

    @Override
    public int getCount()
    {
        return list.size();
    }

    @Override
    public Object getItem(int i)
    {
        return list.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View row = convertView;
        if (row == null)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.day_gridcell, parent, false);
        }

        Button gridcell = (Button) row.findViewById(R.id.gridcell);
        gridcell.setOnClickListener(this);

        TextView textViewUnderscore = (TextView) row.findViewById(R.id.text_view_underscore);
        textViewUnderscore.setVisibility(View.INVISIBLE);

        String[] day_color = list.get(position).split("-");
        String day = day_color[0];
        String month = day_color[1];
        String year = day_color[2];

        LinearLayout evensIconLayout = (LinearLayout) row.findViewById(R.id.events_icon_layout);

        String keyDate = day + "-" + month + "-" + year;

        for (int i = 0; i < eventsList.getEventAmount(keyDate); i++)
        {
            ImageView eventIcon = new ImageView(context);
            eventIcon.setImageResource(R.drawable.ic_access_time);
            LinearLayout.LayoutParams iconLayoutParams = new LinearLayout.LayoutParams(12, 12);
            eventIcon.setLayoutParams(iconLayoutParams);
            evensIconLayout.addView(eventIcon, i);
        }

        gridcell.setText(day);
        gridcell.setTag(day + "-" + month + "-" + year);

        if (day_color[3].equals("GREY"))
        {
            gridcell.setTextColor(Color.LTGRAY);
        }
        if (day_color[3].equals("BLACK"))
        {
            gridcell.setTextColor(Color.BLACK);
        }
        if (day_color[3].equals("BLUE"))
        {
            gridcell.setTextColor(context.getResources().getColor(R.color.colorAppTheme));
            textViewUnderscore.setVisibility(View.VISIBLE);
        }

        if (evensIconLayout.getChildCount() > 0)
        {
            gridcell.setTextColor(context.getResources().getColor(R.color.colorAppTheme));
        }
        return row;
    }

    public int getCurrentDayOfMonth()
    {
        return currentDayOfMonth;
    }

    private void setCurrentDayOfMonth(int currentDayOfMonth)
    {
        this.currentDayOfMonth = currentDayOfMonth;
    }
    public void setCurrentWeekDay(int currentWeekDay)
    {
        this.currentWeekDay = currentWeekDay;
    }
    public int getCurrentWeekDay()
    {
        return currentWeekDay;
    }
}

