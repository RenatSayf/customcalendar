package com.example.renat.customcalendar;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * Created by Renat on 17.03.2017.
 */

public class EventsList
{
    public static String[] groupsName = new String[]{"Приемы пищи", "Спортивное питание", "Замеры"};
    private TreeMap<String, ArrayList<ArrayList<Event>>> map;
    private String monthYear = "-"+CustomCalendar.months[2]+"-2017";

    public EventsList()
    {
        map = new TreeMap<>();
        ArrayList<ArrayList<Event>> groups = new ArrayList<>();

        ArrayList<Event> children1 = new ArrayList<>();
        children1.add(new Event(groupsName[0], "Завтрак", "08:00"));
        children1.add(new Event(groupsName[0], "Обед", "13:00"));
        children1.add(new Event(groupsName[0], "Ужин", "18:00"));
        groups.add(children1);

        for (int i = 5; i < 10; i++)
        {
            map.put(i+monthYear, groups);
        }

        ArrayList<Event> children2 = new ArrayList<>();
        children2.add(new Event(groupsName[1], "Гейнер", "08:30"));
        children2.add(new Event(groupsName[1], "Протеин", "13:30"));
        children2.add(new Event(groupsName[1], "Креатин", "18:30"));
        groups = new ArrayList<>();
        groups.add(children1);
        groups.add(children2);

        for (int i = 12; i < 18; i++)
        {
            map.put(i+monthYear, groups);
        }

        ArrayList<Event> children3 = new ArrayList<>();
        children3.add(new Event(groupsName[2], "Вес", "07:30"));
        children3.add(new Event(groupsName[2], "Пульс", "19:30"));
        groups = new ArrayList<>();
        groups.add(children1);
        groups.add(children2);
        groups.add(children3);

        for (int i = 20; i < 26; i++)
        {
            map.put(i+monthYear, groups);
        }
    }

    public TreeMap<String, ArrayList<ArrayList<Event>>> getEventsShedule()
    {
        return map;
    }

    public int getEventAmount(String key)
    {
        int size;
        try
        {
            size = map.get(key).size();
        } catch (Exception e)
        {
            size = 0;
        }

        return size;
    }
}
